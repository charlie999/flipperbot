#include <Wire.h>
#include <LSM303.h>

#define SERVO_MIN 625
#define SERVO_MAX 2275

String POLL = "POLL";
String REPORT = "REPORT";
String ROTATE = "ROTATE";
String THROTTLE = "THROTTLE";
String ACTUATE = "ACTUATE";

LSM303 compass (0);

String message;

volatile int leftMotor, rightMotor;
volatile double heading;
volatile double compass_heading;

void setup()
{
  Serial.begin(115200);
  compass.init ();
  
  pinMode (13, OUTPUT);
  digitalWrite (13, LOW);
  
  pinMode (2, INPUT);
  pinMode (3, INPUT);
  
  pinMode (5, OUTPUT);
  pinMode (6, OUTPUT);
  pinMode (7, OUTPUT);
  pinMode (8, OUTPUT);
  pinMode (9, OUTPUT);
  pinMode (10, OUTPUT);
  pinMode (11, OUTPUT);
  pinMode (12, OUTPUT);
  
  //8bit timer - pwm, arduino uses this for millis and delay
  TCCR0A = _BV(COM0A1)|_BV(COM0B1)|_BV(WGM00);
  OCR0A = 0x00;
  OCR0B = 0x00;
  
  //16bit timer - servo and control loop
  TCCR1A = 0x00;
  TCCR1B = _BV(CS11);
  OCR1A = clockCyclesPerMicrosecond ()*SERVO_MIN/8;
  OCR1B = clockCyclesPerMicrosecond ()*20000/8;
  TCNT1 = 0x00;
  TIFR1 = _BV(OCF1A)|_BV(OCIE1B);
  TIMSK1 = _BV(OCIE1A)|_BV(OCIE1B);
    
  sei ();
}

void loop()
{
  if (Serial.available ())
  {
    byte temp = Serial.read ();
    
    if (message == "ESC%CONNECT")
    {
      digitalWrite (13, HIGH);
      message = "";
    }
    if (message == "ESC%DISCONNECT")
    {
      digitalWrite (13, LOW);
      message = "";
    }
    
    if (temp != '\n')
    {
      message += (char) temp;
    }
    else
    {
      String command = message.substring (0, message.indexOf (':'));
      String data = message.substring (message.indexOf (':') + 1);
      if (command == POLL)
        poll (data);
      else if (command == REPORT)
        report ();
      else if (command == ROTATE)
        rotate (data);
      else if (command == THROTTLE)
        throttle (data);
      else if (command == ACTUATE)
        actuate (data);
      else
      {
        Serial.print ("ERROR:");
        Serial.println (message);
      }
      message = "";
    }
  }
}

void poll (String data)
{
  Serial.print (POLL + ":");
  Serial.println (data);
}

void report ()
{
  Serial.print (REPORT + ":");
  Serial.print (compass_heading, DEC);
  Serial.print (" ");
  Serial.println (millis (), DEC);
}

void rotate (String data)
{
  char temp[data.length ()];
  data.toCharArray (temp, data.length ());
  heading = atof (temp);
}

void throttle (String data)
{
  char temp[data.length () + 1];
  data.toCharArray (temp, data.length () + 1);
  int val = atoi (temp);
  leftMotor = atoi (temp);
  rightMotor = atoi (temp);
}

void actuate (String data)
{
  String id = data.substring(0, data.indexOf (' '));
  String value = data.substring (data.indexOf (' ') + 1);
  if (value == "ON")
    servoWrite (90);
  else
    servoWrite (0);
}

void servoWrite (int deg)
{
  if (deg < 0)
    deg = 0;
  if (deg > 180)
    deg = 180;
  int temp = map (deg, 0, 180, SERVO_MIN, SERVO_MAX);
  OCR1A = clockCyclesPerMicrosecond ()*temp/8;
}

void setLeftMotor (int value)
{
  if (value > 0)
  {
    digitalWrite (7, LOW);
    digitalWrite (8, HIGH);
  }
  else
  {
    digitalWrite (7, HIGH);
    digitalWrite (8, LOW);
  }
  OCR0A = map (abs (value), 0, 100, 0, 255);
}

void setRightMotor (int value)
{
  if (value > 0)
  {
    digitalWrite (9, LOW);
    digitalWrite (10, HIGH);
  }
  else
  {
    digitalWrite (9, HIGH);
    digitalWrite (10, LOW);
  }
  OCR0B = map (abs (value), 0, 100, 0, 255);
}

ISR (TIMER1_COMPA_vect)
{
  digitalWrite (11, LOW);
}

ISR (TIMER1_COMPB_vect)
{
  TCNT1 = 0;
  digitalWrite (11, HIGH);
  digitalWrite (12, HIGH);
  sei ();
  
  compass_heading = compass.TiltHeading ();
  //update motor values to turn to heading
  
  setLeftMotor (leftMotor);
  setRightMotor (rightMotor);

  digitalWrite (12, LOW);
}
